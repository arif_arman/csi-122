#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int main() {
    srand(time(NULL));
    /*
        Simple array of Sqr of numbers
	*/
    int sqr[10];
    for (int i=0;i<10;i++)
        sqr[i] = i*i;
    
    /* prints array */
    for (int i=0;i<10;i++)
        printf("%d ", sqr[i]);
    printf("\n");
    
    /* scans value to array */
    scanf("%d", &sqr[5]);
    for (int i=0;i<10;i++)
        printf("%d ", sqr[i]);
    

    /*
        useful to hold list information
        say temperature of 10 days
    */
    int arr[10];
    for (int i=0;i<10;i++) arr[i] = rand() % 40;
    /* prints array */
    for (int i=0;i<10;i++)
        printf("%d ", arr[i]);
    printf("\n");

    /* find average temp */
    float avg=0;
    for (int i=0;i<10;i++) avg += arr[i];
    avg /= 10;
    printf("Avg. temp :  %f\n",avg);
    /* find min temp */
    int m = arr[0];
    for (int i=1;i<10;i++) {
        if (arr[i] < m) m = arr[i];
    }
    printf("Min. temp : %d\n",m);

    return 0;
}
