1.

#include<stdio.h>
int main()
{
	int n1,n2;
	scanf("%d %d",&n1,&n2);
	int m = n1 > n2 ? n1 : n2;
	if (n1 == 0 || n2 == 0) printf("%d",0);
	else {
		int factor = m;
		for (int i = m; ;i++) {
			if (n1 % i == 0 && n2 % i == 0)
			{
				factor = i;
				break;
			}
		}
		printf("%d",factor);
	}

	return 0;
}

2.

#include<stdio.h>
int main()
{
	int n;
	scanf("%d",&n);
	int var = 97;
	for (int i=1;i<=2*n-1;i++) {
		printf("%c",var);
		var++;
		if (i==n-1) var = 97;
	}
	return 0;
}
