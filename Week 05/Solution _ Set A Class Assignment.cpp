1.

#include<stdio.h>
int main()
{
	int n1,n2;
	scanf("%d %d",&n1,&n2);
	int m = n1 < n2 ? n1 : n2;
	if (n1 == 0) printf("%d",n2);
	else if (n2 == 0) printf("%d",n1);
	else {
		int factor = 1;
		for (int i = 1; i<=m;i++) {
			if (n1 % i == 0 && n2 % i == 0)
				factor = i;
		}
		printf("%d",factor);
	}

	return 0;
}

2.

#include<stdio.h>
int main()
{
	int n;
	scanf("%d",&n);
	int var = 65;
	for (int i=1;i<=2*n-1;i++) {
		printf("%c",var);
		var++;
		if (i==n) var = 65;
	}
	return 0;
}
