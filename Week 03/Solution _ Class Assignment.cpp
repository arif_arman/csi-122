1.

#include<stdio.h>
int main()
{
	int y;
	scanf("%d",&y);
	if (y%400 == 0 || (y%4==0 && y%100!=0))
		printf("Leap year");
	else
		printf("Not leap year");
	return 0;
}

2.

#include<stdio.h>
int main()
{
	int x,y,z;
	scanf("%d %d %d",&x, &y, &z);
	if (x+y>z && y+z>x && z+x>y)
	{
		if (x==y && y==z)
			printf("Equilateral");
		else if (x == y || y == z || z == x)
			printf("Isoscles");
		else 
			printf("Scalene");
	}
	return 0;
}
