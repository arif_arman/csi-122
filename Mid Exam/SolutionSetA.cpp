#include<stdio.h>
#include<math.h>
int main() {
    /* Task 01 */
    /*
    int n;
    scanf("%d",&n);
    int flag = 0;
    for (int i=2;i<=sqrt(n);i++) {
        if (n%2 == 0) {
            flag = 1;
            break;
        }
    }
    if (flag || n == 1) printf("Not Prime");
    else printf("Prime");
    */

    /* Task 02 */
    /*
    int year;
    scanf("%d",&year);
    int a,b,c,d;
    while(1) {
        year++;
        a = year % 10;
        b = (year / 10) % 10;
        c = (year / 100) % 10;
        d = year / 1000;
        if (a!=b && a!=c && a!=d && b!=c && b!=d && c!=d)
            break;
    }
    printf("%d\n",year);
    */

    /* Task 03 */
    int arr[10];
    for (int i=0;i<10;i++) scanf("%d",&arr[i]);
    int m = arr[0], mPos = 0;
    for (int i=1;i<10;i++) {
        if (arr[i] < m) {
            m = arr[i];
            mPos = i;
        }
    }
    for (int i=mPos;i>0;i--) {
        arr[i] = arr[i-1];
    }
    arr[0] = 0;
    for (int i=0;i<10;i++) printf("%d ", arr[i]);
    return 0;
}
