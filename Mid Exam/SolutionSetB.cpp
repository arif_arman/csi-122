#include<stdio.h>
#include<math.h>
int main() {
    /* Task 01 */
    /*
    int n;
    scanf("%d",&n);
    int factor = n - 1;
    for (;factor >= 1; factor--) {
        if (n%factor == 0)
            break;
    }
    printf("%d\n",factor);
    */

    /* Task 02 */
    /*
    int a,b;
    scanf("%d %d",&a,&b);
    int hamming = 0;
    for (int i=0;i<6;i++) {
        int l = a%10;
        int m = b%10;
        if (l!=m) hamming++;
        a /= 10;
        b /= 10;
    }
    printf("%d\n",hamming);
    */

    /* Task 03 */
    int arr[10];
    for (int i=0;i<10;i++) scanf("%d",&arr[i]);
    int m = arr[0], mPos = 0;
    for (int i=1;i<10;i++) {
        if (arr[i] < m) {
            m = arr[i];
            mPos = i;
        }
    }
    for (int i=mPos;i<9;i++) {
        arr[i] = arr[i+1];
    }
    arr[9] = 0;
    for (int i=0;i<10;i++) printf("%d ", arr[i]);
    return 0;
}
