/* Problem 01 */
#include<stdio.h>
int main() {
	int n;
	scanf("%d",&n);
	int sum = 0;
	for (int i=1;i<=n;i++)
		sum += i*i;
	printf("%d",sum);
	return 0;
}

/* Problem 02 */
#include<stdio.h>
int main() {
	int n;
	scanf("%d",&n);
	int fac = 1;
	for (int i=1;i<=n;i++)
		fac *= i;
	printf("%d",fac);
	return 0;
}

/* Problem 03 */
#include<stdio.h>
int main() {
	int n;
	scanf("%d",&n);
	int fac = 1;
	for (int i=1;i<=n;i++)
		printf("%d * %d = %d\n",i,7,i*7);
	return 0;
}

/* Problem 04 */
#include<stdio.h>
int main() {
	int m,n;
	scanf("%d %d",&m, &n);
	int p = 1;
	for (int i=1;i<=n;i++)
		p = p * m;
	printf("%d",p);
	return 0;
}

/* Problem 05 */
#include<stdio.h>
int main() {
	for (int i=0;i<256;i++)
		printf("%d %c\n",i,i);
	return 0;
}

/* Problem 06 */
#include<stdio.h>
int main() {
	int n;
	scanf("%d",&n);
	int sum = 0;
	for (;n;)
	{
		sum += n%10;
		n /= 10;
	}
	printf("%d",n);
	return 0;
}

/* Problem 07 */
#include<stdio.h>
int main() {
	int n;
	scanf("%d",&n);
	int sum = 0;
	for (int i=1;i<=n;i+=2) {
		sum += 1 / (i*i);
	}
	printf("%d",sum);
	return 0;
}

/* Problem 08 */
#include<stdio.h>
int main() {
	for (int i=1;i<=500;i++) {
		int x = i % 10;
		int y = (i / 10) % 10;
		int Z = (i / 100) % 10;
		if (x*x*x + y*y*y + z*z*z == i) 
			printf("%d",i);
	}
	return 0;
}