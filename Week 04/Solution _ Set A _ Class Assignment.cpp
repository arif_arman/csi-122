1.
#include<stdio.h>
int main() 
{
	int a,b,c,d;
	scanf("%d %d %d %d", &a,&b,&c,&d);
	int min = a;
	if (b < min) min = b;
	if (c < min) min = c;
	if (d < min) min = d;
	printf("%d",min);
	return 0;
}

2.
#include<stdio.h>
int main() 
{
	char ch = getchar();
	switch(ch >= 'a' && ch <= 'z')
	{
		case 0:
			printf("Throw Away");
			break;
		case 1:
			switch(ch)
			{
				case 'a':
				case 'e':
				case 'i':
				case 'o':
				case 'u':
					printf("Vowel");
					break;
				default:
					printf("Consonant");
			}
	}
	return 0;
}