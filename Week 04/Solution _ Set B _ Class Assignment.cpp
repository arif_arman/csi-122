1.
#include<stdio.h>
int main() 
{
	int a,b,c,d;
	scanf("%d %d %d %d", &a,&b,&c,&d);
	int max = a;
	if (b > max) max = b;
	if (c > max) max = c;
	if (d > max) max = d;
	printf("%d",max);
	return 0;
}

2.
#include<stdio.h>
int main() 
{
	char ch = getchar();
	switch(ch >= 'A' && ch <= 'Z')
	{
		case 0:
			printf("Throw Away");
			break;
		case 1:
			switch(ch)
			{
				case 'A':
				case 'E':
				case 'I':
				case 'O':
				case 'U':
					printf("Vowel");
					break;
				default:
					printf("Consonant");
			}
	}
	return 0;
}